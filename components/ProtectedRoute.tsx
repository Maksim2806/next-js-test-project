import { signIn, useSession } from 'next-auth/client'
import { MainLayout } from './MainLayout'

export function ProtecteRoute ({children}) {
  const [session] = useSession()

  if (session) return children
  
  return (
    <MainLayout>
      <h1>Access Denied</h1>
      <h2>You must be signed in to view this page</h2>
      <button onClick={() => signIn()}>Sign In</button>
    </MainLayout>
  )
}