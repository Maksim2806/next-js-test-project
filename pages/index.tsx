import { signIn, signOut, useSession } from 'next-auth/client'
import Link from 'next/link'
import {MainLayout} from '../components/MainLayout'

export default function Index() {
  const [session] = useSession()
  console.log(session);
  
  return (
    <MainLayout title={'Home Page'}>
      <h1>Hello Next.JS!</h1>
      <p><Link href={'/about'}><a>About</a></Link></p>
      <p><Link href="/posts"><a>Posts</a></Link></p>
      <p>Test task for ReactBootcamp!</p>
      <button onClick={() => signIn()}>Log In</button>
      <button onClick={() => signOut()}>Log Out</button>
    </MainLayout>
  )
}