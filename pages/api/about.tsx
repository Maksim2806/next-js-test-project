import {NextApiRequest, NextApiResponse} from 'next'

interface MessageNextApiRequest extends NextApiRequest {
  query: {
    message?: string
  }
}

export default function handler(req: MessageNextApiRequest, res: NextApiResponse) {
  res.statusCode = 200
  res.setHeader('Content-Type', 'application/json')
  console.log('abc');
  res.end(JSON.stringify('dxgfc'))
}