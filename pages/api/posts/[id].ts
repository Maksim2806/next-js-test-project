import { NextApiRequest, NextApiResponse } from "next";

const GetPostsById = async ({query: { id }}: NextApiRequest, res: NextApiResponse) => {
    try {
        const response = await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`)
        const post = await response.json()
        res.json({ post })
      } catch(e) {
        console.error('Error:', e);
      }
}

export default GetPostsById