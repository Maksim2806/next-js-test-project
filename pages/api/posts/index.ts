import {NextApiRequest, NextApiResponse} from 'next'

const GetPosts = async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const response = await fetch('https://jsonplaceholder.typicode.com/posts')
    const posts = await response.json()
    res.json({ posts })
    
  } catch(e) {
    console.error('Error:', e);
  }
}

export default GetPosts