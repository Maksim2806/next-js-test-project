  
import { Provider } from 'next-auth/client'
import NextNprogress from 'nextjs-progressbar'
import '../styles/main.scss'

export default function MyApp({ Component, pageProps }) {
  return (
    <Provider session={pageProps.session}>
      <NextNprogress
        color="#29D"
        startPosition={0.3}
        stopDelayMs={200}
        height={3}
        showOnShallow={true}
        options={{ easing: 'ease', speed: 500 }}
      />
      <Component {...pageProps} />
    </Provider>
  )
}