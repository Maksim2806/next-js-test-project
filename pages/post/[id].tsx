import {MainLayout} from '../../components/MainLayout'
import Link from 'next/link'
import {NextPageContext} from 'next'
import {MyPost} from '../../interfaces/post'

interface PostPageProps {
  post: MyPost
}

export default function Post({ post }: PostPageProps) {
  if (!post) {
    return <MainLayout>
      <p>Loading ...</p>
    </MainLayout>
  }

  return(
    <MainLayout>
      <h1>{post.title}</h1>
      <hr />
      <p>{post.body}</p>
      <Link href={'/posts'}><a>Back to all posts</a></Link>
    </MainLayout>
  )
}

interface PostNextPageContext extends NextPageContext {
  query: {
    id: string
  }
}

export async function getServerSideProps({ query, req }: PostNextPageContext) {

  const response = await fetch(`http://localhost:3000/api/posts/${query.id}`)
  const data = await response.json()

  return {
    props: {post: data.post}
  }
}