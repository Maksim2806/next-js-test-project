import React from 'react'
import Router from 'next/router'
import {MainLayout} from '../../components/MainLayout'
import Image from 'next/image'

export default function About( data) {
 
  const linkClickHandler = () => {
    Router.push('/')
  }

  return (
    <MainLayout title={'About Page'}>
      <pre>{JSON.stringify(data, null, 2)}</pre>
      <button onClick={linkClickHandler}>Go back to home</button>
      <button onClick={() => Router.push('/posts')}>Go to posts</button>
    </MainLayout>
  )
}

About.getInitialProps = async () => {
  const response = await fetch(`https://jsonplaceholder.typicode.com/photos`)
  const data = await response.json()

  return {
    data
  }
}