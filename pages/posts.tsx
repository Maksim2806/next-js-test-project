import {MainLayout} from '../components/MainLayout'
import Link from 'next/link'
import {MyPost} from '../interfaces/post'
import { ProtecteRoute } from '../components/ProtectedRoute'
import { signOut } from 'next-auth/client'

interface PostsPageProps {
  posts: MyPost[]
}

export default function Posts({posts}: PostsPageProps) {
  if (!posts) {
    return <MainLayout>
      <p>Loading ...</p>
    </MainLayout>
  }

  return (
    <ProtecteRoute>
      <MainLayout title="Posts Page">
        <h1>Posts Page</h1>
        <ul>
          {posts.map(post => (
            <li key={post.id}>
              <Link href={`/post/[id]`} as={`/post/${post.id}`}>
                <a>{post.title}</a>
              </Link>
            </li>
          ))}
        </ul>
        <button onClick={() => signOut()}>Sign Out</button>
      </MainLayout>
    </ProtecteRoute>
  )
}

Posts.getInitialProps = async () => {
  const response = await fetch('http://localhost:3000/api/posts')
  const data = await response.json()
  
  return {
    posts: data.posts
  }
}