export interface MyPost {
    id: number
    title: string
    body: string
    userId: number
  }